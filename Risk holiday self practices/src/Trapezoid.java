public class Trapezoid{
	public static void main(String[]args ){
		double top=10.0;
		double bottom=20.5;
		double height=24.4;
		double area=(top+bottom)*(height/2);
		System.out.print("Top: ");
		System.out.println(top);
		System.out.print("Bottom: ");
		System.out.println(bottom);
		System.out.print("Height: ");
		System.out.println(height);
		System.out.print("Area: ");
		System.out.println(area);
	}
}