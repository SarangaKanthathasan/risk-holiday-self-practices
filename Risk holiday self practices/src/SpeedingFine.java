public class SpeedingFine{
	public static void main(String[]args ){
		int[][] list = {
            		{50,35}, 
           		{30,25}, 
            		{60,45}, 
       		 };
		 for (int i=0; i<list.length; i++){
			int speed=list[i][0];
			int limit=list[i][1];
			int fine=(int) ((speed-limit)*20);
                	System.out.println("The fine for driving at "+speed+" mph on a "+limit+" mph road is "+fine+" dollars.");
		}            			
	}
}