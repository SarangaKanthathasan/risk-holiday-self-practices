public class ComputeTaxAndTotal{
	public static void main(String[]args ){
		int sub_total=11050;
		double tax_rate=5.5;
		int tax=(int) (sub_total*(tax_rate/100));
		int total=(int) (sub_total*((100+tax_rate)/100));
		System.out.println("The subtotal = "+(int) (sub_total/100)+" dollars and "+(int) (sub_total-((int)(sub_total/100))*100)+" cents.");
		System.out.println("The tax rate = "+ tax_rate +" percent.");
		System.out.println("The tax = "+(int) (tax/100)+" dollars and "+(int) (tax-((int)(tax/100)*100))+" cents.");
		System.out.println("The total = "+(int)(total/100)+" dollars and "+ (total-((int)(total/100)*100))+" cents.");
	}
}